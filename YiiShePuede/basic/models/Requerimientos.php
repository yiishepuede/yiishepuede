<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "requerimientos".
 *
 * @property integer $idRequerimientos
 * @property string $nombreRequerimiento
 * @property string $actores
 * @property string $descripcion
 * @property double $costoReal
 * @property string $datosUtilizados
 * @property integer $idComplejidadRequerimiento
 * @property integer $idEstadoRequerimiento
 * @property integer $idUsuarioRequerimiento
 * @property integer $idProyectoRequerimiento
 */
class Requerimientos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requerimientos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombreRequerimiento', 'costoReal'], 'required'],
            [['costoReal'], 'number'],
            [['idComplejidadRequerimiento', 'idEstadoRequerimiento', 'idUsuarioRequerimiento', 'idProyectoRequerimiento'], 'integer'],
            [['nombreRequerimiento', 'actores', 'datosUtilizados'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idRequerimientos' => 'Id Requerimientos',
            'nombreRequerimiento' => 'Nombre Requerimiento',
            'actores' => 'Actores',
            'descripcion' => 'Descripcion',
            'costoReal' => 'Costo Real',
            'datosUtilizados' => 'Datos Utilizados',
            'idComplejidadRequerimiento' => 'Id Complejidad Requerimiento',
            'idEstadoRequerimiento' => 'Id Estado Requerimiento',
            'idUsuarioRequerimiento' => 'Id Usuario Requerimiento',
            'idProyectoRequerimiento' => 'Id Proyecto Requerimiento',
        ];
    }
}
