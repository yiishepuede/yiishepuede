<?php

use yii\helpers\Html;
use yii\bootstrap\Collapse;

/**
 * @var yii\web\View $this
 * @var app\models\Requerimientos $model
 */

$this->title = $model->nombreRequerimiento;
$this->params['breadcrumbs'][] = ['label' => 'Requerimientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="requerimientos-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idRequerimientos], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idRequerimientos], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
-->
<div class="Collapse">
<?php
echo '<h1>'.$model->nombreRequerimiento.'</h1>';
echo Collapse::widget([
	// items es un array que permite armar grupos. Cada array representa un elemento o subgrupo
    'items' => [
			'id'=> [ // titulo del elemento
            'content' => $model->idRequerimientos, //contenido del elemento             
			'contentOptions' => ['class' => 'in'], //opciones del contenido. En este caso la Clase 'in' nos permite ver el elemento abierto cuando carga la pagina
			'options'=>['style'=>'border-width: medium; border-color:#000099;font-weight: bolder'] // opciones generales del array.
        ],
			//Nombre Requerimiento
			//'NombreRequerimiento' => [
            //'content' => $model->nombreRequerimiento,
			//'contentOptions' => ['class' => 'in'],
			//'contentOptions'=>['style'=>'background: #AAA; text-align: center'],
			//'options'=>['style'=>'border-width: medium;border-color:#000099']
        //],
			//Actores
			'Actores' => [
			'content' => $model->actores,
			'contentOptions'=>['style'=>'background: #AAA; text-align: center;'],
			'options'=>['style'=>'font-color:red; border-width: medium; border-color:#000099;font-weight: bolder']
		],
			//Descripcion
			'Descripcion' => [
			'content'=> $model ->descripcion,
			'contentOptions'=>['style'=>'background: #AAA; text-align: center'],
			'options'=>['style'=>'border-width: medium;border-color:#000099;font-weight: bolder']
		],
			//Complejidad
			'Complejidad' => [
			'content'=> $model ->idComplejidadRequerimiento,
			'contentOptions'=>['style'=>'background: #AAA; text-align: center'],
			'options'=>['style'=>'border-width: medium;border-color:#000099;font-weight: bolder']
		],
			//Estado
			'Estado' => [
			'content'=> $model ->idEstadoRequerimiento,
			'contentOptions'=>['style'=>'background: #AAA; text-align: center'],
			'options'=>['style'=>'border-width: medium;border-color:#000099;font-weight: bolder']
		],
			//Usuario
			'Usuario' => [
			'content'=> $model ->idUsuarioRequerimiento,
			'contentOptions'=>['style'=>'background: #AAA; text-align: center'],
			'options'=>['style'=>'border-width: medium;border-color:#000099;font-weight: bolder']
		],
			//Proyecto
			'Proyecto' => [
			'content'=> $model ->idProyectoRequerimiento,
			'contentOptions'=>['style'=>'background: #AAA; text-align: center;font-weight: bolder'],
			'options'=>['style'=>'border-width: medium;border-color:#000099']
		],			
    ]
]);
?>
</div>
