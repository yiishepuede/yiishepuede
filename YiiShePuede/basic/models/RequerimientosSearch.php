<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Requerimientos;

/**
 * RequerimientosSearch represents the model behind the search form about `app\models\Requerimientos`.
 */
class RequerimientosSearch extends Requerimientos
{
    public function rules()
    {
        return [
            [['idRequerimientos', 'idComplejidadRequerimiento', 'idEstadoRequerimiento', 'idUsuarioRequerimiento', 'idProyectoRequerimiento'], 'integer'],
            [['nombreRequerimiento', 'actores', 'descripcion', 'datosUtilizados'], 'safe'],
            [['costoReal'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Requerimientos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idRequerimientos' => $this->idRequerimientos,
            'costoReal' => $this->costoReal,
            'idComplejidadRequerimiento' => $this->idComplejidadRequerimiento,
            'idEstadoRequerimiento' => $this->idEstadoRequerimiento,
            'idUsuarioRequerimiento' => $this->idUsuarioRequerimiento,
            'idProyectoRequerimiento' => $this->idProyectoRequerimiento,
        ]);

        $query->andFilterWhere(['like', 'nombreRequerimiento', $this->nombreRequerimiento])
            ->andFilterWhere(['like', 'actores', $this->actores])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'datosUtilizados', $this->datosUtilizados]);

        return $dataProvider;
    }
}
