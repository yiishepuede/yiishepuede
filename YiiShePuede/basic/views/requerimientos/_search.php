<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\RequerimientosSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="requerimientos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idRequerimientos') ?>

    <?= $form->field($model, 'nombreRequerimiento') ?>

    <?= $form->field($model, 'actores') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?= $form->field($model, 'costoReal') ?>

    <?php // echo $form->field($model, 'datosUtilizados') ?>

    <?php // echo $form->field($model, 'idComplejidadRequerimiento') ?>

    <?php // echo $form->field($model, 'idEstadoRequerimiento') ?>

    <?php // echo $form->field($model, 'idUsuarioRequerimiento') ?>

    <?php // echo $form->field($model, 'idProyectoRequerimiento') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
