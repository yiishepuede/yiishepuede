<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Requerimientos $model
 */

$this->title = 'Update Requerimientos: ' . $model->idRequerimientos;
$this->params['breadcrumbs'][] = ['label' => 'Requerimientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idRequerimientos, 'url' => ['view', 'id' => $model->idRequerimientos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="requerimientos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
