<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Requerimientos $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="requerimientos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombreRequerimiento')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'costoReal')->textInput() ?>

    <?= $form->field($model, 'idComplejidadRequerimiento')->textInput() ?>

    <?= $form->field($model, 'idEstadoRequerimiento')->textInput() ?>

    <?= $form->field($model, 'idUsuarioRequerimiento')->textInput() ?>

    <?= $form->field($model, 'idProyectoRequerimiento')->textInput() ?>

    <?= $form->field($model, 'actores')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'datosUtilizados')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
